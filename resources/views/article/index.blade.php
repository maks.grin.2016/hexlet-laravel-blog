
@extends('layouts.app')

@section('content')
    <h1>Список  статей</h1>
    <h3>{{ Session::get('status') }}</h3>
    @foreach ($articles as $article)
        <a href="{{ url(route('articles.show', $article)) }}">
            <h2>{{ $article->name }}</h2>
        </a>
        <a href="{{ url(route('articles.edit', $article)) }}">
            <p>Редактировать статью</p>
        </a>
        <a
            href="{{ url(route('articles.destroy', $article->id)) }}"
            data-confirm="Вы уверены?"
            data-method="delete"
            rel="nofollow"
        >
            Удалить
        </a>
        <div>{{ Str::limit($article->body, 200) }}</div>
    @endforeach
{{--    <div>{{ $articles->links() }}</div>--}}
@endsection
